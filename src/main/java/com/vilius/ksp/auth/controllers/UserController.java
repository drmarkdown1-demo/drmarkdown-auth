package com.vilius.ksp.auth.controllers;

import com.vilius.ksp.auth.dtos.UserInfoDTO;
import com.vilius.ksp.auth.dtos.UserLoginDTO;
import com.vilius.ksp.auth.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.google.common.base.Preconditions.checkNotNull;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;


    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ANONYMOUS','ADMIN')")
    public UserInfoDTO createUser(@RequestBody UserInfoDTO userInfoDTO) {

        checkNotNull(userInfoDTO);

        userService.createUser(userInfoDTO);
        return userInfoDTO;
    }

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAnyRole('ANONYMOUS','USER','ADMIN')")
    public UserInfoDTO getUserInfo(@PathVariable String id, HttpServletRequest request) {


        return userService.retrieveUserInfo(id);


    }

    @PostMapping("/login")
    @PreAuthorize("hasAnyRole('ANONYMOUS')")
    public UserInfoDTO loginUser(@RequestBody UserLoginDTO userLoginDTO) {

        checkNotNull(userLoginDTO);

        return userService.loginUser(userLoginDTO);

    }


//    @DeleteMapping("/delete/{id}")
//    public void deleteUser(@PathVariable String id) {
//
//        log.info("deleting user by id: " + id);
//    }
//
//    @PutMapping("/update/{id}")
//    public UserInfoDTO updateUser(@PathVariable String id, @RequestBody UserInfoDTO userInfoDTO) {
//        log.info("updating user with id: " + id);
//        return null;
//    }

}
