package com.vilius.ksp.auth.controllers;

import com.vilius.ksp.auth.dtos.RoleDTO;
import com.vilius.ksp.auth.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkNotNull;

@RestController
@RequestMapping("/role")
@Slf4j
@PreAuthorize("hasAnyRole('ADMIN')")
public class RoleController {

    @Autowired
    RoleService roleService;

    @PostMapping("/create")
    public RoleDTO createRole(@RequestBody RoleDTO roleDto) {

        checkNotNull(roleDto);

        roleService.createRole(roleDto);
        return roleDto;
    }

    @GetMapping("/info/{id}")
    public RoleDTO getRoleInfo(@PathVariable String id) {

        return roleService.roleInfo(id);
    }
//
//    @DeleteMapping("/{id}")
//    public void deleteRole(@PathVariable String id) {
//
//        log.info("deleting role by id: " + id);
//    }
//
//    @PutMapping("/{id}")
//    public RoleDTO updateRole(@PathVariable String id, @RequestBody RoleDTO roleDTO) {
//
//        log.info("updating user with id: " + id);
//        return null;
//    }
}
