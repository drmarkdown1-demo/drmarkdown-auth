package com.vilius.ksp.auth.initialise;

import com.vilius.ksp.auth.daos.RoleDAO;
import com.vilius.ksp.auth.daos.UserDAO;
import com.vilius.ksp.auth.models.MarkdownRoleModel;
import com.vilius.ksp.auth.models.MarkdownUserModel;
import com.vilius.ksp.auth.services.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

@Profile({"prod"})
@Component
public class InitialiseProdData {

    @Autowired
    UserDAO userDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    TokenService tokenService;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {


        addRoles();
        addUsers();
    }

    private void addUsers() {

        Optional<MarkdownUserModel> adminAccount = userDAO.findByUsername("admin");

        if (!adminAccount.isPresent()) {
            MarkdownUserModel admin = new MarkdownUserModel();
            admin.setUsername("admin");
            admin.setEmail("admin@admin.com");
            admin.setPassword(passwordEncoder.encode("admin"));
            admin.setRoles(Collections.singletonList("ADMIN"));
            tokenService.generateToken(admin);
            admin.setDisplayName("admin");

            userDAO.save(admin);
        }

        Optional<MarkdownUserModel> userAccount = userDAO.findByUsername("user");

        if (!userAccount.isPresent()) {
            MarkdownUserModel user = new MarkdownUserModel();
            user.setUsername("user");
            user.setEmail("user@user.com");
            user.setPassword(passwordEncoder.encode("user"));
            user.setRoles(Collections.singletonList("USER"));
            tokenService.generateToken(user);
            user.setDisplayName("user");

            userDAO.save(user);
        }


    }

    private void addRoles() {


        Optional<MarkdownRoleModel> roleAdmin = roleDAO.findByRole("ADMIN");

        if (!roleAdmin.isPresent()) {
            MarkdownRoleModel adminRole = new MarkdownRoleModel();
            adminRole.setRole("ADMIN");

            roleDAO.save(adminRole);
        }


        Optional<MarkdownRoleModel> roleUser = roleDAO.findByRole("USER");

        if (!roleUser.isPresent()) {
            MarkdownRoleModel userRole = new MarkdownRoleModel();
            userRole.setRole("USER");

            roleDAO.save(userRole);

        }

    }
}
