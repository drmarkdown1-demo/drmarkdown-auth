package com.vilius.ksp.auth.services.impl;

import com.vilius.ksp.auth.daos.RoleDAO;
import com.vilius.ksp.auth.daos.UserDAO;
import com.vilius.ksp.auth.dtos.UserInfoDTO;
import com.vilius.ksp.auth.dtos.UserLoginDTO;
import com.vilius.ksp.auth.models.MarkdownRoleModel;
import com.vilius.ksp.auth.models.MarkdownUserModel;
import com.vilius.ksp.auth.services.TokenService;
import com.vilius.ksp.auth.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UserDAO userDAO;
    @Autowired
    RoleDAO roleDAO;
    @Autowired
    TokenService tokenService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void createUser(UserInfoDTO userInfoDTO) {

        MarkdownUserModel markdownUserModel = modelMapper.map(userInfoDTO, MarkdownUserModel.class);

        markdownUserModel.setPassword(passwordEncoder.encode(markdownUserModel.getPassword()));


        markdownUserModel.setRoles(
                roleDAO.findAll().stream()
                        .map(MarkdownRoleModel::getRole)
                        .filter(role -> role.contains("USER"))
                        .collect(Collectors.toList())
        );

        tokenService.generateToken(markdownUserModel);

        userDAO.save(markdownUserModel);

        userInfoDTO.setPassword("");

        modelMapper.map(markdownUserModel, userInfoDTO);
    }

    @Override
    public UserInfoDTO retrieveUserInfo(String id) {
        Optional<MarkdownUserModel> markdownUserModelOptional = userDAO.findById(id);
        if (markdownUserModelOptional.isPresent()) {
            return modelMapper.map(markdownUserModelOptional.get(), UserInfoDTO.class);
        }
        return null;
    }

    @Override
    public UserInfoDTO loginUser(UserLoginDTO userLoginDTO) {
        Optional<MarkdownUserModel> optionalMarkdownUserModel = userDAO.findByUsername(userLoginDTO.getUsername());
        if (optionalMarkdownUserModel.isPresent()) {
            MarkdownUserModel markdownUserModel = optionalMarkdownUserModel.get();

            if (passwordEncoder.matches(userLoginDTO.getPassword(), markdownUserModel.getPassword())) {

                return modelMapper.map(markdownUserModel, UserInfoDTO.class);
            } else {
                throw new BadCredentialsException("Bad credentials");
            }

        } else {
            throw new BadCredentialsException("Bad credentials");
        }

    }
}
