package com.vilius.ksp.auth.services.impl;

import com.vilius.ksp.auth.daos.RoleDAO;
import com.vilius.ksp.auth.dtos.RoleDTO;
import com.vilius.ksp.auth.models.MarkdownRoleModel;
import com.vilius.ksp.auth.services.RoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDAO roleDAO;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public RoleDTO roleInfo(String id) {

        Optional<MarkdownRoleModel> markdownRoleModelOptional = roleDAO.findById(id);

        if (markdownRoleModelOptional.isPresent()) {
            MarkdownRoleModel markdownRoleModel = markdownRoleModelOptional.get();

            return modelMapper.map(markdownRoleModel, RoleDTO.class);
        }

        return null;
    }

    @Override
    public void createRole(RoleDTO roleDTO) {

        MarkdownRoleModel markdownRoleModel = modelMapper.map(roleDTO, MarkdownRoleModel.class);

        roleDAO.save(markdownRoleModel);

        modelMapper.map(markdownRoleModel, roleDTO);

    }
}
