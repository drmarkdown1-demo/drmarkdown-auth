package com.vilius.ksp.auth.services;

import com.vilius.ksp.auth.dtos.UserInfoDTO;
import com.vilius.ksp.auth.dtos.UserLoginDTO;

public interface UserService {

    void createUser(UserInfoDTO userInfoDTO);

    UserInfoDTO retrieveUserInfo(String id);

    UserInfoDTO loginUser(UserLoginDTO userLoginDTO);

}
