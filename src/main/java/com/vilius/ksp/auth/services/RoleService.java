package com.vilius.ksp.auth.services;

import com.vilius.ksp.auth.dtos.RoleDTO;

public interface RoleService {

    RoleDTO roleInfo(String id);

    void createRole(RoleDTO roleDTO);
}
