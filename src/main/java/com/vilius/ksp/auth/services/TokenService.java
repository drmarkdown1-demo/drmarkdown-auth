package com.vilius.ksp.auth.services;

import com.vilius.ksp.auth.exceptions.InvalidTokenException;
import com.vilius.ksp.auth.models.MarkdownUserModel;

public interface TokenService {

    void validateToken(String jwtToken) throws InvalidTokenException;

    void generateToken(MarkdownUserModel markdownUserModel);
}
