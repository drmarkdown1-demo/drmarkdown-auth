package com.vilius.ksp.auth.config;

import com.vilius.ksp.auth.daos.UserDAO;
import com.vilius.ksp.auth.exceptions.InvalidTokenException;
import com.vilius.ksp.auth.exceptions.MarkdownTokenAuthException;
import com.vilius.ksp.auth.models.MarkdownUserModel;
import com.vilius.ksp.auth.services.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static org.apache.commons.lang3.ObjectUtils.isEmpty;

@Component
public class MarkdownAuthProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    UserDAO userDAO;

    @Autowired
    TokenService tokenService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        final String token = (String) authentication.getCredentials();

        if (isEmpty(token)) {
            return new User(username, "", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
        }

        Optional<MarkdownUserModel> optionalMarkdownUserModel = userDAO.findByJwtToken(token);

        if (optionalMarkdownUserModel.isPresent()) {
            MarkdownUserModel markdownUserModel = optionalMarkdownUserModel.get();

            try {
                tokenService.validateToken(token);
            } catch (InvalidTokenException e) {
                markdownUserModel.setJwtToken(null);
                userDAO.save(markdownUserModel);

                return null;
            }
            return new User(username, "",
                    AuthorityUtils.createAuthorityList(
                            markdownUserModel.getRoles().stream()
                                    .map(roleName -> "ROLE_" + roleName)
                                    .toArray(String[]::new)
                    ));
        }

        throw new MarkdownTokenAuthException("User not found for token: " + token);
    }
}
